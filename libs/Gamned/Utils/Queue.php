<?php

namespace Gamned\Utils;

/**
 * @deprecated
 * Les gens arrétez d'utiliser cette librairie, il faut utiliser la dépendance
 * gamned/job-manager
 */
class Queue {

    protected $_dbAdapter = null;
    protected $_dbTable = null;

    public function setDbAdapter($dbAdapter) {
        $this->_dbAdapter = $dbAdapter;
    }

    public function setDbTable($dbTable) {
        $this->_dbTable = $dbTable;
    }

    /**
     * Ajout d'une tache dans la queue
     * @param string $task
     * @param string(json) $data
     * @param integer $priority
     * @return int|bool id de la tache créée ou false en cas de problème
     */
    public function addTask($task, $advertiserId = null, $data = null, $priority = 0) {
        if (empty($task) || empty($this->_dbAdapter) || empty($this->_dbTable)) {
            return false;
        }

        $q = "INSERT INTO " . $this->_dbTable . " 
            (
            task,
            data,
            annonceurFK,
            treatment,
            last_update_time,
            priority
            ) values (
            '" . $task . "',
            '" . addslashes($data) . "',
            '" . $advertiserId . "',
            'waiting',
            '" . date("Y-m-d H:i:s") . "',
            '" . $priority . "'
        )";

        if (!$this->_dbAdapter->query($q)) {
            return false;
        } else {
            return $this->_dbAdapter->lastInsertId();
        }
    }

    public function getTask($taskId) {
        $query = "SELECT * FROM " . $this->_dbTable . " WHERE id = " . $taskId;

        if (!($stmt = $this->_dbAdapter->query($query))) {
            return false;
        } else {
            $task = $stmt->fetch(\PDO::FETCH_OBJ);
            if (!$task) {
                return false;
            } else {
                return $task;
            }
        }
    }

    /**
     * Récupération de l'état actuel d'une tâche
     * 
     * @param int $taskId
     * 
     * @return string|boolean Statut du traitement ou false
     */
    public function getTaskStatus($taskId) {
        $query = "SELECT treatment FROM " . $this->_dbTable . " WHERE id = " . $taskId;

        if (!($stmt = $this->_dbAdapter->query($query))) {
            return false;
        } else {
            $task = $stmt->fetch(\PDO::FETCH_OBJ);
            if (!$task || !isset($task->treatment)) {
                return false;
            } else {
                return $task->treatment;
            }
        }
    }

    /**
     * Récupération du résultat d'une tâche
     * 
     * @param int $taskId
     * 
     * @return string|boolean Résultat du traitement ou false
     */
    public function getTaskResult($taskId) {
        $query = "SELECT result FROM " . $this->_dbTable . " WHERE id = " . $taskId;

        if (!($stmt = $this->_dbAdapter->query($query))) {
            return false;
        } else {
            $task = $stmt->fetch(\PDO::FETCH_OBJ);
            if (!$task || !isset($task->result)) {
                return false;
            } else {
                return $task->result;
            }
        }
    }

    /**
     * Récupération de la prochaine tache à traiter par type de tache)
     * @param string $task
     * @return array tache récupéré, null si il n'y a pas de taches
     */
    public function getNextTodoTask($task) {
        if (empty($task)) {
            return null;
        }
        //on passe la prochaine tâche sur 1
        $q = "UPDATE " . $this->_dbTable . " set server = '" . \Gamned\Utils\Utils::getMachineName() . "' where server is null and task = '" . $task . "' and treatment = 'waiting' LIMIT 1";
        $this->_dbAdapter->query($q);
        $q2 = "select * from " . $this->_dbTable . " where task = '" . $task . "' and treatment = 'waiting' and server = '" . \Gamned\Utils\Utils::getMachineName() . "' order by priority desc LIMIT 1";
        $stmt = $this->_dbAdapter->query($q2);
        $task = $stmt->fetch(\PDO::FETCH_OBJ);
        return $task;
    }

    /**
     * Récupération de la prochaine tache a traiter par type de tache pour un annonceur pour lequel cette tache ne tourne pas déjà
     * @param string $task
     * @return array tache récupéré, null si il n'y a pas de taches
     */
    public function getNextTodoTaskForFreeAdvertisers($task) {
        if (empty($task)) {
            return null;
        }
        //on recupere la liste des annonceurs pour lesquel une tache est en train de tourner
        $qAdv = "select group_concat(annonceurFK) as advlist from queue_dsp where task = '" . $task . "' and treatment = 'in_progress'";
        $stmtAdv = $this->_dbAdapter->query($qAdv);
        $advs = $stmtAdv->fetch(\PDO::FETCH_OBJ);
        $q = "UPDATE " . $this->_dbTable . " set server = '" . \Gamned\Utils\Utils::getMachineName() . "' where server is null and task = '" . $task . "' and treatment = 'waiting'";
        if (!empty($advs->advlist)) {
            $q .= " and annonceurFK not in (" . $advs->advlist . ")";
        }
        $q .= " LIMIT 1";
        $this->_dbAdapter->query($q);
        $q2 = "select * from " . $this->_dbTable . " where task = '" . $task . "' and treatment = 'waiting' and server = '" . \Gamned\Utils\Utils::getMachineName() . "' order by priority desc LIMIT 1";
        $stmt = $this->_dbAdapter->query($q2);
        $task = $stmt->fetch(\PDO::FETCH_OBJ);
        return $task;
    }

    public function getAllNextTodoTask($task) {
        if (empty($task)) {
            return null;
        }
//on passe la prochaine tâche sur 1
        $q = "UPDATE " . $this->_dbTable . " set server = '" . \Gamned\Utils\Utils::getMachineName() . "' where server is null and  task = '" . $task . "' and treatment = 'waiting'";
        $this->_dbAdapter->query($q);
        $q2 = "select * from " . $this->_dbTable . " where task = '" . $task . "' and treatment = 'waiting' and server = '" . \Gamned\Utils\Utils::getMachineName() . "' order by priority desc";
        $stmt = $this->_dbAdapter->query($q2);
        $task = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $task;
    }

    public function checkIfJobCanRun($tasks) {
        foreach ($tasks as $t) {
            $where = "task = '" . $t["service"] . "'";
            if (!empty($t["adv"])) {
                $where .= " and annonceurFK = " . $t["adv"];
            }
            $q = "select * from " . $this->_dbTable . " where " . $where . " and treatment in ('waiting', 'in_progress') limit 1";
            $stmt = $this->_dbAdapter->query($q);
            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (!empty($data)) {
                return false;
            }
        }
        return true;
    }

    public function releaseTask($task, $excludedId) {
        foreach ($task as $t) {
            if ($t->id == $excludedId) {
                continue;
            }
            $q = "UPDATE " . $this->_dbTable . " set server = null where id = '" . $t->id . "' and treatment = 'waiting'";
            echo $q . "\n";
            $this->_dbAdapter->query($q);
        }
        return true;
    }

    /**
     * Changer le status d'une tache en in_progress
     * @param int $id
     * @param string $result
     * @return null
     */
    public function changeStateToInProgress($id, $result = null) {
        if (empty($id)) {
            return;
        }
        $this->_updateTreatmentState($id, "in_progress", $result);
    }

    /**
     * Changer le status d'une tache en success
     * @param int $id
     * @param string $result
     * @return null
     */
    public function changeStateToSucess($id, $result = null) {
        if (empty($id)) {
            return;
        }
        $this->_updateTreatmentState($id, "success", $result);
    }

    /**
     * Changer le status d'une tache en error
     * @param int $id
     * @param string $result
     * @return null
     */
    public function changeStateToError($id, $result = null) {
        if (empty($id)) {
            return;
        }
        $this->_updateTreatmentState($id, "error", $result);
    }

    /**
     * Fonction ayant pour but d'attendre que les tâches $tastks ne soient plus
     * dans les états $states
     * @param array $tasks Les tâches
     * @param array $states Les états (waiting et in_progress par défaut)
     */
    public function waitUntilTaskEnd($tasks, $advertiser = null, $states = array('waiting', 'in_progress')) {
        do {
            sleep(1);
        } while ($this->hasTasksInState($tasks, $advertiser, $states));
    }

    protected function hasTasksInState($tasks, $advertiser, $states) {
        $tasks_in = $this->stringList($tasks);
        $states_in = $this->stringList($states);

        $sql = 'SELECT COUNT(*) count FROM ' . $this->_dbTable;
        $sql .= ' WHERE task IN (' . $tasks_in . ') AND treatment IN (' . $states_in . ')';
        if (!is_null($advertiser)) {
            $sql .= ' AND annonceurFK = ' . $advertiser;
        }
        $sql .= ';';
        return (boolean) $this->_dbAdapter->query($sql)->fetch(\PDO::FETCH_OBJ)->count;
    }

    /**
     * Produit une chaine utilisable dans une condition SQL IN à partir d'un
     * tableau
     * @param type $values
     */
    protected function stringList($values) {
        $value_string = array_map(function($row) {
            return "'$row'";
        }, $values);

        return join(',', $value_string);
    }

    /**
     * Changement de l'état d'une tache
     * @param integer $id id de la tache
     * @param string $state valeur de l'état ('waiting', 'in_progress', 'success', 'error')
     * @param string $result json de donnée qui contient des informations sur la tache.
     * 
     */
    protected function _updateTreatmentState($id, $state, $result = null) {
        if (empty($this->_dbTable) || empty($this->_dbAdapter)) {
            return false;
        }
        $q = "UPDATE " . $this->_dbTable . " set treatment = '" . $state . "', result = '" . $result . "', last_update_time = '" . date("Y-m-d H:i:s") . "' WHERE id = " . $id;
        $this->_dbAdapter->query($q);
    }

    /**
     * 
     */
    public function checkIfJobExists($task, $advId = null, $state = null) {
        $q = "SELECT count(id) as cnt from " . $this->_dbTable . " where task = '" . $task . "'";
        if (!empty($advId)) {
            $q .= " and annonceurFK = " . $advId;
        }
        if (!empty($state)) {
            $q .= " and treatment = '" . $state . "'";
        }
        $result = $this->_dbAdapter->query($q)->fetch(\PDO::FETCH_OBJ);
        if ($result->cnt == 0) {
            return false;
        }
        return true;
    }

    /**
     * Mise à jour du champ `data` d'une tâche
     * 
     * @param integer $id   id de la tache
     * @param object  $data données à intégrer
     * 
     * @return bool
     */
    public function updateData($id, $data) {
        if (empty($this->_dbTable) || empty($this->_dbAdapter)) {
            return false;
        }

        $sql = "UPDATE " . $this->_dbTable . " 
                SET data = '" . json_encode($data, JSON_FORCE_OBJECT) . "', last_update_time = '" . date("Y-m-d H:i:s") . "' 
                WHERE id = " . $id;
        return (bool) $this->_dbAdapter->query($sql);
    }

}

?>
